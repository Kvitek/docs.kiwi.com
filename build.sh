#!/bin/sh
set -ex

for input_file in *.apib
do
  input_name=${input_file/\.apib/}
  if [ $input_name = "index" ]; then
    mkdir -p public
    output_file=public/index.html
  else
    mkdir -p public/${input_name//-//}
    output_file=public/${input_name//-//}/index.html
  fi

  node_modules/.bin/aglio -i $input_file -o $output_file --theme-template triple streak
done;
