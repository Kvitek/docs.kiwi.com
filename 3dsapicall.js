// Example implementation of 3DS2 with kiwi.com services
// This is happy path only, no error validation - need to be handled

// Dev enviroments
const PCI_API = "https://fe.payments-kiwi-dev.com/frei/";
const CONFIRM_PAYMENT_API =
    "https://booking-pbasista-ywzoww.booking-sandbox.skypicker.com/api/v0.1/confirm_payment";

// Card data from payment form
const CARD_DATA = {
    card: {
        number: "4138490000000000",
        cvv: "123",
        expirationMonth: "01",
        expirationYear: "20",
        holder: "TEST TEST"
    },
    payment: {
        email: "test@kiwi.com",
        gate: "pos",
        order_id: "76293272",
        phone: "000",
        programId: "13032744-a2f9-4444-a817-6e4d8d6f76f6",
        sandbox: true,
        token: "b63e2ecb-f79a-4725-b29e-d37491c2d974"
    }
};

const CONFIRM_PAYMENT_DATA = {
    booking_id: "76293272",
    order_id: "76293272",
    paymentMethodToken: "f6fc7e30-bd18-4b66-9038-0f93be28e886",
    paymentToken: "b63e2ecb-f79a-4725-b29e-d37491c2d974",
    sandbox: true
};

// Helper function
const init3ds = payload =>
    new Promise(resolve => {
        console.log("calling init 3DS with payload", payload);
        window.ftr__.init3DS(payload, (err, res) => {
            resolve(res);
        });
    });

// Helper function
const challenge3ds = threeDsChallenge =>
    new Promise(resolve => {
        console.log("calling trigger challenge if neededpayload", threeDsChallenge);

        const challengeContainer = document.getElementById("challenge-container");
        window.ftr__.triggerChallengeIfNeeded(
            threeDsChallenge,
            challengeContainer,
            async (error, wasChallengePerformed, transStatus, challengeResult) => {
                resolve({
                    error,
                    wasChallengePerformed,
                    transStatus,
                    challengeResult
                });
            }
        );
    });

window.main = async () => {
    // 1. step call save_booking and collect data which are there hard coded in CONFIRM_PAYMENT_DATA variable
    // ...

    // 2. step - call PCI service for tokenization
    const tokenizationResponse = await fetch(PCI_API, {
        method: "POST",
        body: JSON.stringify(CARD_DATA)
    }).then(res => res.json());

    // 3. step - init ftr script
    await init3ds(tokenizationResponse.risk_assessment);

    // 4. step - first call to confirm payment
    const confirmPaymentFirstStepData = {
        payment_details: {
            payment_type: "credit_card",
            status: tokenizationResponse.status,
            token: tokenizationResponse.token,
            encrypted_cvv: tokenizationResponse.encrypted_cvv,
            bin_number: tokenizationResponse.bin_number,
            last_4_digits: tokenizationResponse.last_4_digits,
            holder_name: tokenizationResponse.holder_name,
            expiration: tokenizationResponse.expiration,
            vendor: tokenizationResponse.vendor,
            issuer: tokenizationResponse.issuer,
            country_code: tokenizationResponse.country_code,
            level: tokenizationResponse.level,
            card_type: tokenizationResponse.type,
            pass_luhn_validation: tokenizationResponse.pass_luhn_validation
        },
        threeds_attributes: tokenizationResponse.risk_assessment
    };

    const confirmPaymentResult = await fetch(CONFIRM_PAYMENT_API, {
        method: "POST",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            ...CONFIRM_PAYMENT_DATA,
            ...confirmPaymentFirstStepData
        })
    }).then(res => res.json());
    console.log("confirmPaymentResult", confirmPaymentResult);

    // 5. step - call forter challenge script if 3DS needed (status = 3)
    if (confirmPaymentResult.status === 3) {
        // 3DS 2 required
        const challenge3dsResult = await challenge3ds(
            confirmPaymentResult.three_ds_challenge.encodedChallengeRequest
        );
        console.log("challenge3dsResult", challenge3dsResult);
        const confirmPaymentSecondStepData = {
            after_challenge: true,
            challenge_result: challenge3dsResult.challengeResult
        };

        // 6. step - call confirm payment again with new attributes from successfull challenge
        const confirmPaymentSecondResult = await fetch(CONFIRM_PAYMENT_API, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                ...CONFIRM_PAYMENT_DATA,
                ...confirmPaymentFirstStepData,
                ...confirmPaymentSecondStepData
            })
        }).then(res => res.json());
        console.log("confirmPaymentSecondResult", confirmPaymentSecondResult);

        // Confirm payment should return 0 or 1
        if (confirmPaymentSecondResult.status === 0) {
            // Success!
        }
    }
};