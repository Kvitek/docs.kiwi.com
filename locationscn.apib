FORMAT: 1A
HOST: https://api-skypicker.ccindex.cn/

# Locations API

::: note
'Locations' is a simple API used to search, suggest and resolve locations in various situations.
:::

## Locations Collection [/]

### Locale setting
Every search in this collection accepts `locale`
GET argument which sets output locale to your specified locale.
This is optional and defaults to locale 'en'.

Available locales:

|       |       |       |       |       |       |
|------ |------ |------ |------ |------ |------ |
| ar-AE | cs-CZ | da-DK | de-DE | el-GR | en-US |
| es-ES | fi-FI | fr-FR | hu-HU | is-IS | it-IT |
| iw-IL | ja-JP | ko-KR | lt-LT | nl-NL | no-NO |
| pl-PL | pt-BR | pt-PT | ro-RO | ru-RU | sk-SK |
| sr-RS | sv-SE | th-TH | tr-TR | uk-UA | zh-CN |
| zh-TW |


### Output location types
Every search in this collection accepts `location_types` list, which is optional and decides
what types of locations will be on the output. If not specified the output will contain all types, such as
`airport`, `station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region` and `continent`.
You are able to specify one or more location types by appending more `&location_types=` to your query string.

### Limiting the output
Every search request has a `limit` parameter to set maximum number of results. The default is 20.

### Sorting the output
All requests type except id and slug requests can have the output sorted according
to specified `sort` parameters.
Multiple types of sortings are supported:
+ `name` - localized according to specified locale.
+ `rank` - importance/popularity of location (locale dependent)
+ `_type` - type of the location, e.g. `airport`

The default sorting order is set to 'ascending'.  Descending order can be set by adding a minus sign in front of the parameter, eg. `-rank`.
Multiple sorting parameters can be set. The order of their appearance in url determines priority in sorting.

**Please note**
* Only active parameters have been described in this document. Parameters that are not described are deprecated.
* All URLs must be encoded (for example use `%3B` instead of `;`)
* <span style="color:red">**The responses are G-zipped and need to be unpacked (response header Content-Encoding: gzip).**</span>
* These characters should not be found in IDs: `:.?!*\'\"„"ʻ()+&@#%~|`


### Search by query [GET /locations?term={term}&locale={locale}&location_types={location_types}&limit={limit}&active_only={active_only}&sort={sort}]
Type of request used mainly for suggestions (based on incomplete names)

+ Parameters
    + term (required, string, `PRG`) ... searched term (for suggestions).  This parameter expects a full IATA code.
    If IATA code is not given, the search will go through other available fields: 'name' or 'code' of the
        location.
        It also depends on the 'location_types' specified eg. airport, city, country.  The search that is used
        behind the scenes is elasticsearch.  It returns data based on relevancy and many other factors.
    + locale (optional, string, `en-US`) ... desired locale output - this is the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + location_types (optional, list, `airport`) ... desired location output, accepted values:
    `station`, `airport`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`. To use more than one `location_types`, use multiple `&location_types=`
    + limit (optional, integer, `10`) ... default value = 10. Desired number of results in the output.
    + active_only (optional, string, `true`) ... default value = true.  It displays all active locations.
    + sort (optional, string, `name`) - desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes

### Search by radius [GET /locations?type=radius&term={term}&lat={lat}&lon={lon}&radius={radius}&locale={locale}&location_types={location_types}&limit={limit}&sort={sort}&active_only={active_only}]
This type of request supports either specification of location by coordinates (`lat`, `lon`) or by identifier (slug or id of location - `term`)

+ Parameters
    + lat (optional, float, `40.730610`) ... latitude of the centre point of the search.  `40.7` is also acceptable.
    + lon (optional, float, `-73.935242`) ... longitude of the centre point of the search.  `-73.9` is also acceptable.
    + term (optional, string, `london_gb`) ... identifier of the location - slug or id.  **You cannot specify
            coordinates & term in the same request.**
    + radius (optional, float, `250`) ... the radius defaults to 250 km but can be changed
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + location_types (optional, list, `airport`) ... desired location output, accepted values:
    `station`, `airport`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`. To use more than one `location_types`, use multiple `&location_types=`
    + limit (optional, integer, `20`) ... default value = 20. Desired number of results in the output.
    + sort (optional, string, `name`) - desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displays all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing requests parameters, invalid parameter values
    + Attributes
+ Response 404
    Location specified by `term` (id or slug) does not exist
    + Attributes

### Search by box [GET /locations?type=box&low_lat={low_lat}&low_lon={low_lon}&high_lat={high_lat}&high_lon={high_lon}&locale={locale}&location_types={location_types}&limit={limit}&sort={sort}&active_only={active_only}]
Get all locations within the specified box.

+ Parameters
    + low_lat (required, float, `40.200610`) ...  latitude of southwest corner of geo box search; `40.2` is also acceptable.
    + low_lon (required, float, `-74.624328`) ... longitude of southwest corner of geo box search; `-74.6` is also acceptable.
    + high_lat (required, float, `44.763212`) ... latitude of northeast corner of geo box search; `44.7` is also acceptable.
    + high_lon (required, float, `-73.376543`) ... longitude of northeast corner of geo box search; `-73.3` is also acceptable.
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + location_types (optional, list, `airport`) ... desired location output, accepted values:
    `station`, `airport`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`. To use more than one `location_types`, use multiple `&location_types=`
    + limit (optional, integer, `20`) ... default value = 20. Desired number of results in the output.
    + sort (optional, string, `name`) ... desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displayes all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes

### Get by subentity [GET /locations?type=subentity&term={term}&locale={locale}&active_only={active_only}&location_types={location_types}&limit={limit}&sort={sort}]
Get all locations that are below (in hierarchy) the one specified by id - e.g. for `?type=subentity&term=london_gb` all locations in London are returned (as London is `city`, `airports`, `stations` and `bus_stations` are returned).

+ Parameters
    + term (required, string, `ZW`) ... this is the exact IATA airport or ISO3166 location code - station, airport, bus\_station, city, autonomous_territory, subdivision, country
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + location_types (optional, list, `airport`) ... desired location output, accepted values:
    `station`, `airport`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`. To use more than one `location_types`, use multiple `&location_types=`
    + limit (optional, integer, `20`) ... default value = 20. Desired number of results in the output.
    + sort (optional, string, `name`) ... desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displayes all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes
+ Response 404
    Location specified by `term` (id) does not exist
    + Attributes

### Get by id [GET /locations?type=id&id={id}&locale={locale}&active_only={active_only}]
Get location specified by its id.

+ Parameters
    + id (required, string, `ZW`) ... this is the exact IATA airport or ISO3166 location code - `station`, `airport`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`.
        Multiple ids can be specified by appending `&id={id}`.
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + active_only (optional, string, `true`) ... default value = true.  It displayes all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes

### Get by anything [GET /locations?type=general&key={key}&value={value}]
Get locations specified by any of the following fields for example: id, int_id, code, icao, slug.

+ Parameters
    + key (required, string, `int_id`) ... key is the field in response.  To be used in conjunction with the 'value' field.
    + value (required, string, `1555`) ... value of the field selected in key.  To be used in conjunctions with the 'key' field.  It returns locations that match the specified conditions.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes

### Get dump [GET /locations?type=dump&locale={locale}&location_types={location_types}&limit={limit}&sort={sort}&active_only={active_only}]
Get dump of locations data in specified language.

+ Parameters
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + location_types (optional, list, `airport`) ... desired location output, accepted values:
      `station`, `airport`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`. To use more than one `location_types`, use multiple `&location_types=`
    + limit (optional, integer, `20`) ... default value = 20. Desired number of results in the output.
    + sort (optional, string, `name`) ... desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displayes all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes

### Search top destinations [GET /locations?type=top\_destinations&term={term}&locale={locale}&limit={limit}&sort={sort}&active\_only={active\_only}&source\_popularity={source\_popularity}&fallback\_popularity={fallback\_popularity}]
This type of request returns a list of destinations most searched / clicked on / booked from the starting point `term`.  The `limit` is used to limit the range of results.

+ Parameters
    + term (required, string, `london_gb`) ... identifier of the start point - slug or id (`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`)
        More than one is possible.
    + locale (optional, string, `en-US`) ... desired locale output - this is the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + limit (optional, integer, `100`) ... default value = 100. Desired number of results in the output.
    + sort (optional, string, `sort=name`) - desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displays all active locations.
    + source_popularity (optional, string, `searches`) ... based on `searches` (default), `bookings` or `clicks`
    + fallback_popularity (optional, string, `bookings`) ... based on `searches`, `bookings` or `clicks`.  Can be left blank.  Used if not enough results is returned by `source_popularity`.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes
+ Response 404
    Location specified does not exist
    + Attributes


### Search by hashtags [GET /locations?type=hashtag&hashtag={hashtag}&term={term}&month={month}&locale={locale}&limit={limit}&sort={sort}&active\_only={active\_only}]
This type of request returns a list of destinations most searched / clicked on / booked from the starting point `term`.  The `limit` is used to limit the range of results.

+ Parameters
    + hashtag (required, string, `beach`) ... hashtag that the returned location have to be tagged with.
    + term (optional, string, `london_gb`) ... identifier of the location the returned locations should be part of - id (`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`).
    + month (optional, integer, `1`) ... the month in which the hashtag should be valid. Multiple values are allowed.
    + locale (optional, string, `en-US`) ... desired locale output - this is the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + limit (optional, integer, `100`) ... default value = 100. Desired number of results in the output.
    + sort (optional, string, `name`) - desired order of the output.  For A->Z use 'sort=name', for Z->A use 'sort=-name'.
    + active_only (optional, string, `true`) ... default value = true.  It displays all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes
+ Response 404
    Location specified does not exist
    + Attributes


### Top destinations` hashtags lookup [GET /locations?type=top\_hashtags&term={term}&limit={limit}&source_popularity={source_popularity}&fallback_popularity={fallback_popularity}]
This type of request returns a list of destinations' hashtags most searched / clicked on / booked from the starting point `term`. In other words it can be understood as following: What are the most popular activies at the places that customers tend to search for / click / book when flying from `term` ?
The `limit` is used to limit the range of results.

+ Parameters
    + term (required, string, `london_gb`) ... identifier of the start point - id (`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`)
        More than one is possible.
    + limit (optional, integer, `100`) ... default value = 100. Desired number of results in the output.
    + source_popularity (optional, string, `searches`) ... based on `searches` (default), `bookings` or `clicks`
    + fallback_popularity (optional, string, `bookings`) ... based on `searches`, `bookings` or `clicks`.  Can be left blank.  Used if not enough results is returned by `source_popularity`


+ Response 200 (application/json)
    + Attributes
        + hashtags (array[string]) - array of top hashtags for destinations
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes
+ Response 404
    Location specified does not exist
    + Attributes


## Slugs Collection [/?type=slug]

### Search by seo url [GET /locations?type=slug&term={term}&locale={locale}&active_only={active_only}]
+ Parameters
    + term (required, string, `albany-new-york-united-states`) ... this field expects the exact slug code of the `airport`, `station`, `bus_station`, `city`, `autonomous_territory`, `subdivision`, `country`, `region`, `continent`.
    + locale (optional, string, `en-US`) ... desired locale output - this is the the language of the results.
        Should any other locale be used other than the specified locales, en-US is used.
    + active_only (optional, string, `true`) ... default value = true.  It displayes all active locations.


+ Response 200 (application/json)
    + Attributes
        + locations (array[`airport`, `station`, `bus_station`, `city`, `subdivision`, `autonomous_territory`, `country`, `region`, `continent`])
        + meta
            + locale (string) - locale used
            + status (string) - information if the locale has been found and used
+ Response 400
    Bad request format, missing request's params, invalid params' values
    + Attributes
+ Response 404
    Location specified by `term` (slug) does not exist
    + Attributes


# Data Structures


## airport (object)
+ id: LCY
+ int_id: 9625
+ active: true (boolean)
+ code: LCY (string)
+ name: London City Airport (string)
+ slug: `london-city-airport` (string)
+ alternative_names (array)
+ rank: 0
+ timezone: Europe/London (string)
+ city (object)
    + id: london_gb (string)
    + name: London (string)
    + code: LON (string)
    + slug: london (string)
    + subdivision: null
    + autonomous_territory: null
    + country (object)
        + id: GB (string)
        + name: United Kingdom (string)
        + slug: `united-kingdom` (string)
        + code: GB (string)
    + region (object)
        + id: northern-europe (string)
        + name: Northern Europe (string)
        + slug: `northern-europe` (string)
    + continent (object)
        + id: europe (string)
        + name: Europe (string)
        + slug: `europe` (string)
        + code: EU (string)
+ location (object)
    + lon: `0.054167`
    + lat: `51.505`
+ alternative_departure_points (array)
    + id: LHR
    + distance: 35.8
    + duration: 1.4
+ type: airport


## city (object)
+ id: london_gb (string)
+ active: true (boolean)
+ name: London (string)
+ slug: `london-united-kingdom` (string)
+ code: LON (string)
+ alternative_names (array)
+ rank: 0
+ timezone: Europe/London (string)
+ population: 7556900
+ airports: 6
+ stations: 0
+ subdivision: null
+ autonomous_territory: null
+ country (object)
    + id: GB (string)
    + name: United Kingdom (string)
    + slug: `united-kingdom` (string)
    + code: GB (string)
+ region (object)
    + id: northern-europe (string)
    + name: Northern Europe (string)
    + slug: `northern-europe` (string)
+ continent (object)
    + id: europe (string)
    + name: Europe (string)
    + slug: `europe` (string)
    + code: EU (string)
+ location (object)
    + lat: `51.507351`
    + lon: `-0.127758`
+ type: city


## subdivision (object)
+ name: California (string)
+ id: CA_US (string)
+ active: true (boolean)
+ name: California
+ slug: `california-united-states` (string)
+ code: CA (string)
+ alternative_names: `Kalifornie`(array)
+ rank: 0
+ country (object)
    + id: US (string)
    + code: US (string)
    + name: United States (string)
    + slug: `united-states`
+ region (object)
    + id: northern-america (string)
    + name: Northern America (string)
    + slug: `northern-america` (string)
+ continent (object)
    + id: north-america (string)
    + code: NA (string)
    + name: North America (string)
    + slug: `north-america` (string)
+ location (object)
    + lat: `38.555605`
    + lon: `-121.468926`
+ type: subdivision


## country (object)
+ id: GB (string)
+ active: true (boolean)
+ code: GB
+ name: United Kingdom (string)
+ slug: `united-kingdom`
+ alternative_names: `Velká Británie"` (array)
+ rank: 9
+ neighbours: `IE` (array)
+ organizations: `EU` (array)
+ region (object)
    + id: northern-europe (string)
    + name: Northern Europe (string)
    + slug: `northern-europe` (string)
+ continent (object)
    + id: europe (string)
    + code: EU (string)
    + name: Europe (string)
    + slug: `europe` (string)
+ location (object)
    + lat: `51.5`
    + lon: `-0.083333`
+ type: country

## `autonomous_territory` (object)
+ id: PR (string)
+ active: true (boolean)
+ name: Puerto Rico (string)
+ slug: `puerto-rico-united-states`
+ code: PR
+ alternative_names (array)
+ rank: 0
+ country (object)
    + id: US (string)
    + code: US (string)
    + name: United States (string)
    + slug: `united-states` (string)
+ region (object)
    + id: caribbean (string)
    + name: Caribbean (string)
    + slug: `caribbean` (string)
+ continent (object)
    + id: north-america (string)
    + code: NA (string)
    + name: North America (string)
    + slug: `north-america` (string)
+ location (object)
    + lat: `18.45`
    + lon: `-66.1`
+ type: autonomous_territory


## station (object)
+ id: XAK (string)
+ active: true (boolean)
+ code: XAK (string)
+ name: Herning Railway Station (string)
+ slug: `erning-railway-station-herning-denmark`
+ alternative_names (array)
+ rank: 127
+ timezone: Europe/Copenhagen (string)
+ city (object)
    + id: herning_dk (string)
    + name: Herning (string)
    + code: ZRY (string)
    + slug: `herning-denmark` (string)
    + subdivision: null
    + autonomous_territory: null
    + country (object)
        + id: DK (string)
        + code: DK (string)
        + name: Denmark (string)
        + slug: `denmark` (string)
    + region (object)
        + id: nothern-europe (string)
        + name: Northern Europe (string)
        + slug: `northern-europe` (string)
    + continent (object)
        + id: europe (string)
        + name: Europe (string)
        + slug: `europe` (string)
        + code: EU (string)
+ location (object)
    + lat: `56.132625`
    + lon: `8.977992`
+ alternative_departure_points (array)
    + id: KRP
    + distance: 0.4611565747
    + duration: 0.82
+ type: station



## `bus_station` (object)
+ id: PFL (string)
+ active: true (boolean)
+ code: PFL (string)
+ name: Florenc (string)
+ slug: `florenc-prague-czech-republic`
+ alternative_names (array)
+ rank: 127
+ timezone: Europe/Prague (string)
+ city (object)
    + id: prague_cz (string)
    + name: Prague (string)
    + code: PRG (string)
    + slug: `prague-czechia` (string)
    + subdivision: null
    + autonomous_territory: null
    + country (object)
        + id: CZ (string)
        + code: CZ (string)
        + name: Czechia (string)
        + slug: `czechia` (string)
    + region (object)
        + id: central-europe (string)
        + name: Central Europe (string)
        + slug: `central-europe` (string)
    + continent (object)
        + id: europe (string)
        + name: Europe (string)
        + slug: `europe` (string)
        + code: EU (string)
+ location (object)
    + lat: `50.132625`
    + lon: `14.977992`
+ alternative_departure_points (array)
    + id: BRQ
    + distance: 200
    + duration: 2.5
+ type: bus_station


## region (object)
+ id: central_europe (string)
+ active: true (boolean)
+ name: Central Europe (string)
+ slug: `central-europe`
+ alternative_names: [] (array)
+ rank: 127
+ continent (object)
    + id: europe (string)
    + code: EU (string)
    + name: Europe (string)
    + slug: `europe` (string)
+ type: region


## continent (object)
+ id: europe (string)
+ active: true (boolean)
+ code: EU (string)
+ name: Europe (string)
+ slug: `europe` (string)
+ alternative_names: [] (array)
+ rank: 127
+ type: continent


## Slug (object)
+ type: slug,
+ slug: edinburg,
+ id: edinburg_us_1
