FORMAT: 1A

# Migrating to 3DS2

## Introduction

**PSD2 (Payment Services Directive)** regulations are coming into effect from **14th September 2019**. **PSD2** regulations are taking effect only in **Europe**. Regulations present **Strong Customer Authentication (SCA)**, which is helping to protect customers and reduce payment fraud.

One of the ways how to adapt **PSD2** is to implement **3D-Secure 2.0 (3DS2)** to **payment checkout flow**.

This document describes how to apply described changes with Kiwi.com.

Note that this document gives a high level overview over solution.

## Due date

* PSD2 regulation in place: 14th September

## Requirements

* Use our PICI service for tokenization instead of Tactus or PaymentOS (POS). See our sandbox swagger for basic specifications:

    <https://pici-docs.finance-sandbox.skypicker.com/api/ui/>

* Use Forter script: Forter’s script provides solution for front end part of 3DS2. This prerequisite applies only if you use Kiwi.com's MPI.

## Implementation

We support 2 ways to perform 3DS implementation:
 - Use your [merchant plug-in (MPI)](https://en.wikipedia.org/wiki/Merchant_plug-in): you can process 3DS with your MPI on your own and send Kiwi.com result of verification through standard API.
 - Integrate **Kiwi.com MPI** into your payment flow.

## Partner's Merchant plug-in (MPI)

<div style="width: 640px; height: 480px; margin: 10px; position: relative;">
    <iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/6137521a-8998-4190-b9bc-142e391af3e1" id="Rp9OAYnj8.H7">
    </iframe>
</div>

[Link to diagram source](https://www.lucidchart.com/documents/view/6137521a-8998-4190-b9bc-142e391af3e1)

### Changes in the flow

The only change needed is to pass 3DS verification result through confirm_payment API.

API has optional field `three_d_secure_attributes`, which may contain these attributes:
* `version` - string in format `2.X.X`
* `ds_trans_id` - string. Universally unique transaction identifier assigned by the DS to identify a single transaction. Canonical format as defined in IETF RFC 4122.
* `av` - string (base64). Authentication value used as proof of authentication. (sometimes called `cavv` as a reference to 3DSv1)
* `av_algorithm` - string. Authentication value algorithm.
* `eci` - enum `[00, 01, 02, 05, 06, 07]`. The Electronic Commerce Indicator.
* `trans_status` - enum: `[ A, Y, N, R, U, C]` aka * `authentication_status` or `status`. Indicates whether a transaction qualifies as an authenticated transaction or account verification
* `trans_status_reason` - string(2). Provides information on why the Transaction Status field has the specified value. Example: "03"
* `mode`- enum `[sca, frictionless]`. Applied authentification mode

## Kiwi.com’s MPI Flow diagram

<div style="width: 640px; height: 480px; margin: 10px; position: relative;">
    <iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/b05b8e03-48be-44b5-8ee6-68db1a75c942" id="7VGDF3ru7.HI">
    </iframe>
</div>

[Link to diagram source](https://www.lucidchart.com/invitations/accept/49b7aa26-5610-418e-8957-7d4c3c73b28a)

## Flow description

1. After a customer starts the payment process, save_booking API has to be called with order details. We are now sending more data in the response of save_booking.

2. [Tokenization service](https://pici-docs.finance-sandbox.skypicker.com/api/ui/) with credit card data is called. Credit card is tokenized during this step; also 3DS session is initialized.

    Endpoint URL:

    ```
    https://fe.payments-kiwi.com/frei/
    ```

    <details><summary>
    Request example:
    </summary>

    ```
    {
    “card”: {
        “number”: “4111111111111111”,
        “cvv”: “123”,
        “expirationMonth”: “01”,
        “expirationYear”: “20”,
        “holder”: “TEST TEST”
    },
    “payment”: {
        “email”: “test@kiwi.com”,
        “gate”: “pos”,
        “order_id”: “76830985",
        “phone”: “000",
        “programId”: “c07d9a69-7560-4b7b-9959-a73cecd58e92",
        “sandbox”: true,
        “token”: “3695747f-d40f-4018-b9c1-0494237a6497”
        }
    }
    ```

    </details>

    <details><summary>
    Response example:
    </summary>

    ```
    {
        "status": "success",
        "token": "a896ba53-3057-4ce2-9901-a954966d9d4e",
        "encrypted_cvv": "ZjBjOTIwYjUtNWEwMS00ZDk4LWE0ZjEtYTk3YTE5NDdlNmZjY3Z2X2RlbGltaXRlcnZhdWx0OnYxOkpPVUU2ekg3V1daYzJ6dDM3T2FtWG94V0YzL2dobUtRRXhmSW5YVDlVZktCcE82UUx5YTlnc3ltRE9DQUxIQVR3NS95dUNiTHAwWncrZkRKd1VkSEpYZUdmQlRURFgrMVJoWW92bDFo",
        "bin_number": "413849",
        "last_4_digits": "0000",
        "holder_name": "N/A N/A",
        "expiration": "2020-01-01",
        "vendor": "VISA",
        "issuer": null,
        "country_code": "FRA",
        "level": "",
        "type": "CREDIT",
        "pass_luhn_validation": true,
        "risk_assessment": {
            "correlationId": "2019-09-09T132405400-8f850304-v3",
            "threeDSServerTransID": "7fdf0fdf-8548-44ff-adb0-f099efdf9038",
            "methodURL": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/3ds_method",
            "version": "2.1",
            "status": "success"
        }
    }
    ```

    </details>

3. This step is new in 3DS, pass `risk_assesment` object from the previous API call into `ftr__.init3DS` function, which is provided by Forter in their JS script. This function creates hidden iframe on front end, which sends browser data to credit card Issuer. See specifications below.

    <details><summary>
    Initializing `ftr__.init3DS` script with payload:
    </summary>

    ```javascript
    ftr__.init3DS({
      "correlationId": "2019-09-09T140102113-97157ab9-v3",
      "threeDSServerTransID": "f98a7239-7f35-4101-b4a6-e1ee36fb10ea",
      "methodURL": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/3ds_method",
      "version": "2.1",
      "status": "success"
    }, (err, res) => {})
    ```

    </details>

    <details><summary>
    The `risk_assessment` is similar to the following example:
    </summary>

    ```
    "risk_assessment": {
           "correlationId": "2019-09-09T132405400-8f850304-v3",
           "threeDSServerTransID": "7fdf0fdf-8548-44ff-adb0-f099efdf9038",
           "methodURL": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/3ds_method",
           "version": "2.1",
           "status": "success"
       }
    ```
    </details>

4. `confirm_payment` API has to be called, but with additional data taken from tokenization call (step 2. in this flow description). The `risk_assessment` must be renamed to threeds_attributes. See the example below with expanded request of confirm payment. If 3DS challenge is needed, API call returns appropriate data. For more see the [confirm_payment API documentation](https://docs.kiwi.com/booking/#payment-confirm_payment-post).

    - Requires new parameter `threeDSServerTransID`
    - Requires new parameter `correlationId`
    - Accepts **optional** boolean parameter `after_challenge` with value `False`

    <details><summary>
    Request body:
    </summary>

    ```
    {
        "booking_id": "76109550",
        "paymentToken": "5816161a-4b7f-472e-a2f9-2be38bafad3a",
        "paymentMethodToken": "bf1f85e9-2a6a-4bc9-94bc-33288220c96d",
        "sandbox": true,
        "payment_details": {
            "payment_type": "credit_card",
            "status": "success",
            "token": "bf1f85e9-2a6a-4bc9-94bc-33288220c96d",
            "encrypted_cvv": "MTkxNDhjYWYtNWRlOC00YTNlLWFiZjMtOTZhNzU2YTVlNGY1Y3Z2X2RlbGltaXRlcnZhdWx0OnYxOktGSmRPWlY3eGw2dTNoOE1hWWFFaG9qK0gybWVDRXpIcTh0cmFsdDIzekt4U1FIRzEvWlA4VmsvcGRoR0h3SGsxTElTOEpqaE9oNVRDSFFJL3BQVC90NXhLcnlveTdYUk9LcXdyKytu",
            "bin_number": "413849",
            "last_4_digits": "0000",
            "holder_name": "N/A N/A",
            "expiration": "2020-01-01",
            "vendor": "VISA",
            "issuer": null,
            "country_code": "FRA",
            "level": "",
            "card_type": "CREDIT",
            "pass_luhn_validation": true
        },
        "threeds_attributes": {
            "correlationId": "2019-09-09T140102113-97157ab9-v3",
            "threeDSServerTransID": "f98a7239-7f35-4101-b4a6-e1ee36fb10ea",
            "methodURL": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/3ds_method",
            "version": "2.1",
            "status": "success"
        },
        "order_id": "76109550",
        "after_challenge": false,
        "challenge_result": null
    }
    ```
    </details>

    <details><summary>
    Response example:
    </summary>

    ```json
    {
        "status": 3,
        "msg": "action needed",
        "three_ds_challenge": {
            "ACSUrl": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/browser_sdk_challenges",
            "encodedChallengeRequest": "eyJhY3NUcmFuc0lEIjoiODEzYTY2MzktYjM0YS00N2JlLTg4ZTctMTRkZWNhZjExZWQ2IiwiY2hhbGxlbmdlV2luZG93U2l6ZSI6IjA0IiwibWVzc2FnZVZlcnNpb24iOiIyLjEuMCIsIm1lc3NhZ2VUeXBlIjoiQ1JlcSIsInRocmVlRFNTZXJ2ZXJUcmFuc0lEIjoiZWE2M2Y5OWQtYTZmYS00MTFkLWFmOGEtMzk1Y2E1NGIwYjBlIn0=",
            "version": "2.1"
        }
    }
    ```

    </details>

    There can be four possible response statuses:

    * **0** - Success. Payment done, no action needed.
    * **1** - Success. 3DS1 flow.
    * **2** - Async payment, pulling payment status needed.
    * **3** - 3DS2 challenge needed.

5. In case 3DS challenge is needed (`confirm_payment` response has `"status": 3`), `ftr__.triggerChallengeIfNeeded` function provided by Forter’s script needs to be called. This function requires front end HTML element as an input parameter to which issuer’s iframe with 3DS challenge is loaded. After the customer finishes the challenge, the function returns challenge result.

    <details><summary>
    Implent the following script:
    </summary>

    ```javascript
    ftr__.triggerChallengeIfNeeded(
      {
        ACSUrl: "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/browser_sdk_challenges",
        encodedChallengeRequest:
          "eyJhY3NUcmFuc0lEIjoiZjE0NDhkNjItZmJjOS00MzdjLTgxMm…zNWFlNDQtMjdmMi00ODQzLTk4MmQtZTc3ZmVkMzkxZDEyIn0=",
        version: "2.1",
      },
      challengeContainer,
      async (error, wasChallengePerformed, transStatus, challengeResult) => {},
    )
    ```

    </details>

6. With the result of the previous step, `confirm_payment` API is called again. The API returns final status of the payment. For more see the [confirm_payment API documentation](https://docs.kiwi.com/booking/#payment-confirm_payment-post).

    - requires boolean parameter `after_challenge` with value **True**
    - requires new parameter `challenge_result`

    <details><summary>
    Request example:
    </summary>

    ```
    {
        "booking_id": "76111167",
        "paymentToken": "adc23b17-09e5-44ce-ad17-b29c947a97f4",
        "paymentMethodToken": "a2d4e772-8d3b-4e76-be9e-9c9489022420",
        "sandbox": true,
        "payment_details": {
            "payment_type": "credit_card",
            "status": "success",
            "token": "a2d4e772-8d3b-4e76-be9e-9c9489022420",
            "encrypted_cvv": "MWY5ZWM2MjUtNTFlYy00ZjlmLTg0YjMtOThiZDBhY2M0NzQ4Y3Z2X2RlbGltaXRlcnZhdWx0OnYxOkgyWHhhTlcrV1MvRGNRT3A4Unl4WGM1dXBrbzdhM3RiNFpyNG94dkJ0WHI3elU4YmNDcW5FdzZDeVRBUVk0dGlsNU0yUWJWOUFqclpGcHQrVnI0VTRhZlR1dHZJdkgvSFh1bFlia3Qy",
            "bin_number": "413849",
            "last_4_digits": "0000",
            "holder_name": "N/A N/A",
            "expiration": "2020-01-01",
            "vendor": "VISA",
            "issuer": null,
            "country_code": "FRA",
            "level": "",
            "card_type": "CREDIT",
            "pass_luhn_validation": true
        },
        "threeds_attributes": {
            "correlationId": "2019-09-09T140558910-1ef6e6d0-v3",
            "threeDSServerTransID": "98777992-37dd-4caa-ad7f-0e3fe76cd535",
            "methodURL": "https://acs-us-east-1.nd.nds-sandbox-issuer.com/api/v1/acs/3ds_method",
            "version": "2.1",
            "status": "success"
        },
        "order_id": "76111167",
        "after_challenge": true,
        "challenge_result": "eyJhY3NUcmFuc0lEIjoiYzVkYmRmOTQtODUxNi00ZDI2LTkyZmUtZjgwMTg2YTZlYWYyIiwidHJhbnNTdGF0dXMiOiJZIiwibWVzc2FnZVZlcnNpb24iOiIyLjEuMCIsIm1lc3NhZ2VUeXBlIjoiQ1JlcyIsInRocmVlRFNTZXJ2ZXJUcmFuc0lEIjoiOTQ1MzA1ZTMtZmVhYS00NjkxLWIxNmYtMTAwZGIzNTdjMTBiIn0="
    }
    ```

    </details>

## Changes in comparison with the old payment flow

* Prior to this update, the second step described in the [Flow description](https://docs.kiwi.com/3DS2_migration/#header-flow-description) was **payment tokenization** through Zooz’s API. With this update, Kiwi.com wrapped this communication into new API and also added 3DS initialization mechanism. This means that old Zooz’s API is replaced with Kiwi’s API, which has more functionality.

* With this update, there are two new steps. The third step (calling `ftr__.init3DS` function) and the fifth step (calling `ftr__.triggerChallengeIfNeeded` function), both are required for 3DS2 flow. These functions are part of Forter’s JS script. They serve for direct communication between browser and issuer.

* With this update, the `confirm_payment` requires more data. In some cases, instead of returning payment status, confirm_payment can lead to a 3DS challenge. Afterwards, the `confirm_payment` is called again (with a result of the challenge).

## Implementation example

Download the **index.html** and **index.js** zipped <a href="/downloadable/3ds2-test.zip">here</a>. Unzip the files and open **index.js** in a text editor. This example shows only the happy path steps of 3DS2 flow.

### Testing the 3DS2 flow locally

To run the testing script locally:

1. Download the **index.html** and **index.js** zipped <a href="/downloadable/3ds2-test.zip">here</a>.
2. Unzip the files.
3. In a terminal, navigate to a directory where you unzipped the files. Run the following command:

    ```
    npx http-server
    ```

4. Open `http://localhost:8080/` in a browser. Click on **run 3DS2 flow**, open developer's console (**ctrl**/**command** **shift** **i**) and see the request and the response.
